package com.app.pabstaengineerindia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ZoneSelection extends AppCompatActivity {

    ListView list;
    String[] listItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zone_selection);

        list = (ListView) findViewById(R.id.zones);
        listItem = getResources().getStringArray(R.array.Zones);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.activity_listview,listItem);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String value=adapter.getItem(position);
                Intent obj = new Intent(ZoneSelection.this, DepotSelection.class);
                Bundle args = new Bundle();
                args.putString("zone",value+"_Locations");
                obj.putExtras(args);
                startActivity(obj);
            }
        });
    }
}
