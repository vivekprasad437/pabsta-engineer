package com.app.pabstaengineerindia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class DepotSelection extends AppCompatActivity {


    ListView list;
    String[] listItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_depot_selection);

        list = (ListView) findViewById(R.id.depot);
        Bundle bundle = getIntent().getExtras();
        String stuff = bundle.getString("zone");
        int zoneId = getResources().getIdentifier(stuff, "array", this.getPackageName());
        listItem = getResources().getStringArray(zoneId);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.activity_listview,listItem);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String value=adapter.getItem(position);
                Intent obj = new Intent(DepotSelection.this, FormFill.class);
                Bundle args = new Bundle();
                args.putString("Depot",value);
                obj.putExtras(args);
                startActivity(obj);
            }
        });
    }
}
