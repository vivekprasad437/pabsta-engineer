package com.app.pabstaengineerindia;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class FormFill extends AppCompatActivity implements View.OnClickListener {

    ImageButton btnDatePicker, btnTimePicker;
    TextView txtDate, txtTime;
    int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_fill);


        btnDatePicker = (ImageButton) findViewById(R.id.img_planner);
        btnTimePicker = (ImageButton) findViewById(R.id.img_clock);
        txtDate = (TextView) findViewById(R.id.in_date);
        txtTime = (TextView) findViewById(R.id.in_time);

        txtDate.setOnClickListener(this);
        txtTime.setOnClickListener(this);
        btnDatePicker.setOnClickListener(this);
        btnTimePicker.setOnClickListener(this);
    }
       @Override
        public void onClick(View v) {

            if (v == txtDate || v== btnDatePicker) {

                // Get Current Date
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            }

            if (v == txtTime || v== btnTimePicker) {

                // Get Current Time
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);

                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                String hrr;
                                if(hourOfDay>12) {
                                   hourOfDay=hourOfDay-12;
                                   hrr="PM";
                                }
                                else
                                   hrr="AM";

                                if(minute<=9 && hourOfDay<=9)
                                    txtTime.setText("0"+hourOfDay + ":" + "0"+minute + " " + hrr);
                                else if(minute<=9 )
                                    txtTime.setText(hourOfDay + ":" + "0"+minute+" "+hrr);
                                else if(hourOfDay<=9)
                                    txtTime.setText("0"+hourOfDay + ":" + minute+" "+hrr);
                                else
                                    txtTime.setText(hourOfDay + ":" + minute+" "+hrr);
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        }
}
